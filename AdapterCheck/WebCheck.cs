﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Net.Sockets;
using System.Windows;

namespace AdapterCheck
{
    public class WebCheck
    {
        public bool StatusOk(string uri)
        {
            try
            {
                return (((HttpWebResponse)((HttpWebRequest)WebRequest.Create(uri)).GetResponse()).StatusCode == HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
