﻿using AdapterCheck.Helpers;
using Newtonsoft.Json;
using System;
using System.Threading;
using System.Windows;
using System.Web.Script.Serialization;
using System.IO;
using System.Text;
namespace AdapterCheck
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ExportDelegate _exportDelegate;
        private ViewModel _vm;
        private JavaScriptSerializer _jsonSerializer;
        private const string _optionFile = @"..\..\SavedData\LastUsedOption.json";
        private delegate void ExportDelegate();

        public MainWindow()
        {
            InitializeComponent();
            _jsonSerializer = new JavaScriptSerializer();
            _vm = new ViewModel(this);
            DataContext = _vm;
            if (!File.Exists(_optionFile))
                (new FileInfo(_optionFile)).Create();
            var sr = new StreamReader(_optionFile);
            var savedOptions=sr.ReadToEnd();
            if(!string.IsNullOrEmpty(savedOptions))
            {
                var deserialized = _jsonSerializer.Deserialize(savedOptions,typeof(LastUsedOption)) as LastUsedOption ;
                AdapterUrl.Text=deserialized.AdapterUrl;
                PeriodicityBox.Text=deserialized.Periodicity;
                ReceiversMailBox.Text=deserialized.ReceiversEmail;
                SmtpServerBox.Text=deserialized.SmtpServerAddress;
                PortBox.Text=deserialized.Port;
                UseSslBox.IsChecked=deserialized.UseSsl;
                SendersEmailBox.Text=deserialized.SendersEmail;
                SenderPassBox.Password = deserialized.SenderPass;
        }
            sr.Close();

        }


        private void button_Click(object sender, RoutedEventArgs e)
        {
            UpdateModel();

            if (_vm.ErrorCount == 0)
            {
                _vm.AddEvent(new CheckingEvent(EventType.CheckingStarted, EventResult.Success, string.Empty));
                button.IsEnabled = false;
                AdapterUrl.IsEnabled = false;
                    PeriodicityBox.IsEnabled = false;
                    ReceiversMailBox.IsEnabled = false;
                    SmtpServerBox.IsEnabled = false;
                    PortBox.IsEnabled = false;
                    UseSslBox.IsEnabled = false;
                    SendersEmailBox.IsEnabled = false;
                    SenderPassBox.IsEnabled = false;
                button_Copy.IsEnabled = false;

                //var check = new WebCheck().CheckConnection(_vm);

               var check = true;

               
                var sw = new StreamWriter(_optionFile);
                var lastSavedData = new LastUsedOption
                {
                    AdapterUrl = AdapterUrl.Text,
                    Periodicity = PeriodicityBox.Text,
                    ReceiversEmail = ReceiversMailBox.Text,
                    SmtpServerAddress = SmtpServerBox.Text,
                    Port = PortBox.Text,
                    UseSsl = UseSslBox.IsChecked.GetValueOrDefault(),
                    SendersEmail = SendersEmailBox.Text,
                    SenderPass = SenderPassBox.Password
                };
                var serialized = _jsonSerializer.Serialize(lastSavedData);
                sw.Write(serialized);
                sw.Close();
                if (check)
                {
                    _exportDelegate = new ExportDelegate(Check);
                    _exportDelegate.BeginInvoke(new AsyncCallback(Callback), null);
                }
            }
            else
            {
                MessageBox.Show("Все поля обязательны для заполнения.");
            }
        }

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            UpdateModel();

            try
            {
                new EmailSend().CheckConnection(_vm);
            }
            catch (Exception exception)
            {
                //    MessageBox.Show("Ошибка при отправке сообщения");
            }
        }

        private void UpdateModel()
        {
            _vm.ErrorCount = 0;
            _vm.ErrorText = String.Empty;

            _vm.Uri = AdapterUrl.Text;
            _vm.Host = SmtpServerBox.Text;

            int result1;
            int.TryParse(PeriodicityBox.Text, out result1);
            _vm.TimePeriod = result1;

            int result2;
            int.TryParse(PortBox.Text, out result2);
            _vm.Port = result2;

            _vm.SenderName = SendersEmailBox.Text;
            _vm.SenderPass = SenderPassBox.Password;
            _vm.Ssl = UseSslBox.IsChecked;
            _vm.RecipientName = ReceiversMailBox.Text;

            if (_vm.ErrorCount != 0)
            {
                MessageBox.Show(_vm.ErrorText);
            }

        }

        private void Callback(IAsyncResult ar)
        {
            _exportDelegate.EndInvoke(ar);
        }


        public void Check()
        {
            while (true)
            {
                bool flag = new WebCheck().StatusOk(_vm.Uri);
                
                    _vm.AddEvent(new CheckingEvent(EventType.AdapterCheck, flag ? EventResult.Success : EventResult.Error, String.Empty));
                
                if (!flag)
                {
                    try
                    {
                       var rr = new EmailSend().SendEmail(_vm);
                       
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Ошибка при отправке сообщения");
                    }
                    ///_vm.AppTitle = DateTime.Now.ToShortTimeString() + (flag ? (_vm.AppTitle += " Сервер доступен") : _vm.AppTitle += " Сервер недоступен");
                }
                //_vm.AppTitle = DateTime.Now.ToShortTimeString() + " - Сервер доступен: " + flag.ToString();

                Thread.Sleep(1000 * 60 * _vm.TimePeriod); 
            }
            // ReSharper disable once FunctionNeverReturns
        }
    }
}
