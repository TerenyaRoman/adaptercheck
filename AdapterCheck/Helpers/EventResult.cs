﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterCheck.Helpers
{
    public enum EventResult
    {
        Error = 0,
        Success = 1,
        TimeOut = 2
    }
}
