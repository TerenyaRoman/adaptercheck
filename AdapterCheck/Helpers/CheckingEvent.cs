﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterCheck.Helpers
{
    public class CheckingEvent : INotifyPropertyChanged
    {
        public CheckingEvent(EventType eventTypeEnum, EventResult eventResultEnum,string note)
        {
            EventDate = System.DateTime.Now.ToString("HH:mm:ss MM.dd.yyyy");
            EventTypeEnum = eventTypeEnum;
            EventResultEnum = eventResultEnum;
            Note = note;
        }
        private string _eventDate;
        public string EventDate
        {
            get
            {
                return _eventDate;
            }
            private set
            {
                _eventDate = value;
                OnPropertyChanged("EventDate");
            }
        }
        public EventType EventTypeEnum { get; private set; }
        public string EventTypeName
        {
            get
            {
                switch ((int)EventTypeEnum)
                {
                    case 0: return "Запуск службы проверки";
                    case 1: return "Проверка работоспособности адаптера";
                    case 2: return "Отправка оповещения об отсутствии отклика адаптера";
                    default: return string.Empty;
                }
            }
        }
        public EventResult EventResultEnum { get; private set; }
        public string EventResultName
        {
            get
            {
                switch ((int)EventResultEnum)
                {
                    case 0: return "Ошибка";
                    case 1: return "Успешно";
                    case 2: return "Таймаут";
                    default: return string.Empty;
                }
            }
        }
        public string Note { get;private set; }
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
