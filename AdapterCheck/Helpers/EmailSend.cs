﻿using AdapterCheck.Helpers;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Windows;
using System.Threading;

namespace AdapterCheck
{
    public class EmailSend
    {
        public bool SendEmail(ViewModel model)
        {
            try
            {
                var client = new SmtpClient(model.Host, model.Port)
                {
                    Credentials = new NetworkCredential(model.SenderName, model.SenderPass)
                };
                if (model.Ssl.HasValue)
                {
                    client.EnableSsl = model.Ssl.Value;
                }
                var message = new MailMessage
                {
                    From = new MailAddress(model.SenderName)                    
                };
                message.To.Add(new MailAddress(model.RecipientName));
                message.Subject = "Адаптер";
                message.Body = "Адаптер СМДО - ИИС: Проверьте работоспособность Адаптера.";
                client.Send(message);
                    model.AddEvent(new CheckingEvent(EventType.MsgSended, EventResult.Success, string.Empty));
                
                return true;
            }
            catch (Exception e)
            {
                model.AddEvent(new CheckingEvent(EventType.MsgSended, EventResult.Error, e.InnerException.ToString()));

                return false;
            }
        }



        public bool CheckConnection(ViewModel model)
        {
            try
            {
                var client = new SmtpClient(model.Host, model.Port)
                {
                    Credentials = new NetworkCredential(model.SenderName, model.SenderPass)
                };
                if (model.Ssl.HasValue)
                {
                    client.EnableSsl = model.Ssl.Value;
                }
                var message = new MailMessage
                {
                    From = new MailAddress(model.SenderName)
                };
                message.To.Add(new MailAddress(model.RecipientName));
                message.Subject = "Адаптер, тестовое сообщение";
                message.Body = "Адаптер СМДО - ИИС: Тестовое сообщение";
                client.Send(message);
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка при отправке сообщения");

                return false;
            }
        }


    }
}
