﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterCheck.Helpers
{
    public class LastUsedOption
    {
        public string AdapterUrl { get; set; }
        public string Periodicity { get; set; }
        public string ReceiversEmail { get; set; }
        public string SmtpServerAddress { get; set; }
        public string Port { get; set; }
        public bool UseSsl { get; set; }
        public string SendersEmail{get;set;}
        public string SenderPass { get; set; }
    }
}
