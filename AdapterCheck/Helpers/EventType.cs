﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterCheck.Helpers
{
    public enum EventType
    {
        CheckingStarted=0,
        AdapterCheck = 1,
        MsgSended = 2,
    }
}
