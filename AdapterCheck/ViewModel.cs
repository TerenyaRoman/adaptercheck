﻿using AdapterCheck.Helpers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Remoting.Contexts;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;

namespace AdapterCheck
{
    [Synchronization()]
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel(MainWindow view)
        {
            CurrView=view;
            _listOfEvents = new List<CheckingEvent>();
        }
        public MainWindow CurrView { get; private set; }
        private string _appTitle = "AdapterCheck";

        private string _host;
        private int _port;
        private string _recipientName;
        private string _senderName;
        private string _senderPass;
        private string _uri;
        private int _timePeriod;

        public int ErrorCount;
        public string ErrorText = string.Empty;

        /// <summary>
        /// Периодичность проверки
        /// </summary>
        public int TimePeriod
        {
            get
            {
                return _timePeriod;
            }
            set
            {
                if (value == 0)
                {
                    ErrorCount++;
                    ErrorText = string.Empty;
                    ErrorText += ("Заполните поле: Периодичность проверки(мин) \r\n");
                }
                if (value.ToString().Length > 3 && value.ToString().Length == 0)
                {
                    ErrorCount++;
                    ErrorText += ("Поле 'Периодичность проверки(мин)' не может быть больше 3 символов или пустым \r\n");
                }
                if (_timePeriod != value)
                {
                    _timePeriod = value;
                    OnPropertyChanged("TimePeriod");
                }
            }
        }

        /// <summary>
        /// Адрес SMTP сервера
        /// </summary>
        public string Host
        {
            get
            {
                return _host;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    ErrorCount++;
                    ErrorText += ("Заполните поле: Адрес SMTP сервера \r\n");
                }
                if (value.Length > 100)
                {
                    ErrorCount++;
                    ErrorText += ("Поле 'Адрес SMTP сервера' не может быть больше 100 символов \r\n");
                }
                if (_host != value)
                {
                    _host = value;
                    OnPropertyChanged("Host");
                }
            }
        }

        /// <summary>
        /// Порт
        /// </summary>
        public int Port
        {
            get
            {
                return _port;
            }
            set
            {
                if (value == 0)
                {
                    ErrorCount++;
                    ErrorText += ("Заполните поле: Порт \r\n");
                }
                if (value.ToString().Length > 6 && value.ToString().Length == 0)
                {
                    ErrorCount++;
                    ErrorText += ("Поле 'Порт' не может быть больше 6 символов или пустым \r\n");
                }
                if (_port != value)
                {
                    _port = value;
                    OnPropertyChanged("Port");
                }
            }
        }


        /// <summary>
        /// Email получателя
        /// </summary>
        public string RecipientName
        {
            get
            {
                return _recipientName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    ErrorCount++;
                    ErrorText += ("Заполните поле: Email получателя \r\n");
                }
                if (value.Length > 100 || value.Length < 6)
                {
                    ErrorCount++;
                    ErrorText += ("Поле 'Email получателя' не может быть больше 100 или меньше 6 символов \r\n");
                }
                if (_recipientName != value)
                {
                    _recipientName = value;
                    OnPropertyChanged("RecipientName");
                }
            }
        }

        /// <summary>
        /// Отправитель
        /// </summary>
        public string SenderName
        {
            get
            {
                return _senderName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    ErrorCount++;
                    ErrorText += ("Заполните поле: Отправитель \r\n");
                }
                if (value.Length > 100 || value.Length < 6)
                {
                    ErrorCount++;
                    ErrorText += ("Поле 'Отправитель' не может быть больше 100 или меньше 6 символов \r\n");
                }
                if (_senderName != value)
                {
                    _senderName = value;
                    OnPropertyChanged("SenderName");
                }
            }
        }

        /// <summary>
        /// Пароль
        /// </summary>
        public string SenderPass
        {
            get
            {
                return _senderPass;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    ErrorCount++;
                    ErrorText += ("Заполните поле: Пароль \r\n");
                }
                if (value.Length > 100)
                {
                    ErrorCount++;
                    ErrorText += ("Поле 'Пароль' не может быть больше 100 символов \r\n");
                }
                if (_senderPass != value)
                {
                    _senderPass = value;
                    OnPropertyChanged("SenderPass");
                }
            }
        }
        private List<CheckingEvent> _listOfEvents;
        public List<CheckingEvent> ListOfEvents
        {
            get
            {
                return _listOfEvents;
            }
            set
            {
                _listOfEvents = value;
                OnPropertyChanged("ListOfEvents");
            }
        }

        /// <summary>
        /// SSL
        /// </summary>
        public bool? Ssl;

        /// <summary>
        /// URL Адаптера
        /// </summary>
        public string Uri
        {
            get
            {
                return _uri;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    ErrorCount++;
                    ErrorText += ("Заполните поле: URL Адаптера \r\n");
                }
                if (value.Length > 100 || value.Length < 6)
                {
                    ErrorCount++;
                    ErrorText += ("Поле 'URL Адаптера' не может быть больше 100 или меньше 6 символов \r\n");
                }
                if (_uri != value)
                {
                    _uri = value;
                    OnPropertyChanged("Uri");
                }
            }
        }

        /// <summary>
        /// Заголовок приложения
        /// </summary>
        public string AppTitle
        {
            get { return _appTitle; }
            set
            {
                if (_appTitle != value)
                {
                    _appTitle = value;
                    OnPropertyChanged("AppTitle");
                }
            }
        }
        public void AddEvent(CheckingEvent checkingEvent)
        {
            _listOfEvents.Add(checkingEvent);
            var itemSource = CurrView.EventsList.ItemsSource;
            /*Dispatcher.CurrentDispatcher.BeginInvoke(new ThreadStart(
                delegate
                {
                    CurrView.EventsList.ItemsSource = _listOfEvents;
                    
                    MessageBox.Show("11111111111111111");
                }));*/
            OnPropertyChanged("ListOfEvents");
            //CurrView.EventsList.Items.Add(checkingEvent);
            /*CollectionViewSource.GetDefaultView(
            CurrView.EventsList.ItemSource).Refresh();*/
            
        }
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}

